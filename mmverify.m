function status = mmverify(p, answer, relerr)
%
% testsuite.m
%
% Development script for maxmod.m that checks
% that maxmod() is getting the right answers to
% the right relative accuracy
%
% J.J.Green 2008

    [M, t, h, evals] = maxmod(p, relerr);

    actualRelerr = abs(M-answer)/min([M, answer]);

    fprintf('%3i %6.4e %5i\t', numel(p), relerr, evals);

    if actualRelerr>relerr

        fprintf('failed\n');
        fprintf('required relative error < %e\n', relerr);
        fprintf('actual relative error = %e\n\n', actualRelerr);
        status = 1;

    else

        fprintf('passed\n');
        status = 1;

    end

end
