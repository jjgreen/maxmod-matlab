default:

test-matlab :
	matlab -nodisplay -r testsuite

test-octave :
	octave --no-gui --eval testsuite
