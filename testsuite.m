% -*- matlab -*-
% testsuite.m
% 
% testsuite for the matlab/octave implemention of maxmod
%
% J.J.Green 2008, 2012, 2015

fprintf('maxmod matlab/octave %s testsuite\n', version);

p = 0;
n = 0;

% check that maxmod() is getting the right answers to
% the right relative accuracy

% simple case

a = [1, 1, 1];
p = p + mmverify(a, 3, 1e-3);
p = p + mmverify(a, 3, 1e-6);
p = p + mmverify(a, 3, 1e-9);
n = n + 3;

% slightly bigger

a = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1];
p = p + mmverify(a, 2, 1e-3);
p = p + mmverify(a, 2, 1e-6);
p = p + mmverify(a, 2, 1e-9);
p = p + mmverify(a, 2, 1e-12);
n = n + 4;

% large values

p = p + mmverify(1e12*a, 2e12, 1e-12);
n = n + 1;

% complex values

p = p + mmverify(1i*a, 2, 1e-12);
n = n + 1;

% time Consuming

a      = zeros(600, 1);
a(1)   = 30;
a(600) = 1;
p = p + mmverify(a, 31, 1e-3);
p = p + mmverify(a, 31, 1e-6);
p = p + mmverify(a, 31, 1e-9);
p = p + mmverify(a, 31, 1e-12);
n = n + 4;

% beta0 bug regression, with the bug this take 7e6
% poly evaluation, without it takes none.

a = [1, 0, 0];
p = p + mmverify(a, 1, 1e-12);
n = n + 1;

fprintf('\nTotal number of tests: %i\n', n);
fprintf('Passed: %i\n', p);
fprintf('Failed: %i\n', n-p);

if n == p
    exit(0)
else
    exit(1)
end
